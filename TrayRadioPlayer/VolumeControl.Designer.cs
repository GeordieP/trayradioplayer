﻿namespace TrayRadioPlayer {
    public partial class VolumeControl {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.MuteBtn = new System.Windows.Forms.Button();
            this.VolumeSlider = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.VolumeSlider)).BeginInit();
            this.SuspendLayout();
            // 
            // MuteBtn
            // 
            this.MuteBtn.Location = new System.Drawing.Point(3, 4);
            this.MuteBtn.Name = "MuteBtn";
            this.MuteBtn.Size = new System.Drawing.Size(39, 28);
            this.MuteBtn.TabIndex = 0;
            this.MuteBtn.Text = "Mute";
            this.MuteBtn.UseVisualStyleBackColor = true;
            this.MuteBtn.Click += new System.EventHandler(this.MuteBtn_Click);
            // 
            // VolumeSlider
            // 
            this.VolumeSlider.AutoSize = false;
            this.VolumeSlider.LargeChange = 1;
            this.VolumeSlider.Location = new System.Drawing.Point(41, 3);
            this.VolumeSlider.Maximum = 100;
            this.VolumeSlider.Name = "VolumeSlider";
            this.VolumeSlider.Size = new System.Drawing.Size(138, 23);
            this.VolumeSlider.TabIndex = 1;
            this.VolumeSlider.TickFrequency = 25;
            this.VolumeSlider.Scroll += new System.EventHandler(this.VolumeSlider_Scroll);
            // 
            // VolumeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(177, 35);
            this.Controls.Add(this.VolumeSlider);
            this.Controls.Add(this.MuteBtn);
            this.Name = "VolumeControl";
            this.Text = "VolumeControl";
            ((System.ComponentModel.ISupportInitialize)(this.VolumeSlider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button MuteBtn;
        private System.Windows.Forms.TrackBar VolumeSlider;
    }
}