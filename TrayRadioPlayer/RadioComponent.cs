﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Un4seen.Bass;

namespace TrayRadioPlayer {
    public class RadioComponent {

        MainMenu mainMenu;

        // Booleans
        private bool isPlaying;
        private bool isMuted;

        // BASS Related Variables
        private int stream = 0;

        // Misc
        private float masterVolume = 1.0f;     // current volume level (not affected by mute)

        // Stream URL Strings
        private string techhouse = "http://pub1.di.fm:80/di_techhouse";
        private string dubstepfm = "http://72.232.40.182:80";

        public RadioComponent(MainMenu mainMenu) {
            this.mainMenu = mainMenu;

            // BASS Related things 
            Un4seen.Bass.BassNet.Registration("pkerkidhd@gmail.com", "2X2520919152222");
            Bass.BASS_Free();


            // Booleans
            isPlaying = false;
            isMuted = false;
        }

        public void PlayStop() {
            isPlaying = !isPlaying;

            if (isPlaying) {
                if (Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero)) {
                    stream = Bass.BASS_StreamCreateURL(techhouse, 0, BASSFlag.BASS_DEFAULT, null, IntPtr.Zero);
                    Bass.BASS_ChannelPlay(stream, false);
                    Console.WriteLine(Bass.BASS_ChannelGetTagsMETA(stream)[0]);
                }
            } else {
                Bass.BASS_StreamFree(stream);
                Bass.BASS_Free();
                stream = 0;
            }
        }

        public void ChangeVolume(float volume) {
            Bass.BASS_ChannelSetAttribute(stream, BASSAttribute.BASS_ATTRIB_VOL, volume);
            
            if (!isMuted) this.masterVolume = volume;       // only update volume if we're not muted, to avoid volume toggling between 0 and 0 upon muting
        }

        public void ToggleMute() {
            if (isMuted) {
                isMuted = !isMuted;
                ChangeVolume(this.masterVolume);
            } else {
                isMuted = !isMuted;     // redundant, but this needs to be placed in here or else: either it gets toggled before the if and the if doesn't fire (if this line is placed before the if), or changeVolume doesn't work correctly because it thinks we aren't muted (if this line placed after the if)
                ChangeVolume(0.0f);
            }
        }
    }
}