﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrayRadioPlayer {
    public partial class VolumeControl : Form {

        public RadioComponent gRadio;

        public VolumeControl(RadioComponent radio) {
            InitializeComponent();
            this.gRadio = radio;
        }

        private void VolumeSlider_Scroll(object sender, EventArgs e) {
            gRadio.ChangeVolume((float)VolumeSlider.Value / 100);
        }

        private void MuteBtn_Click(object sender, EventArgs e) {
            gRadio.ToggleMute();
        }
    }
}
