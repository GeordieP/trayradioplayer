﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrayRadioPlayer {
    public class TrayRadio : Form {

        private MainMenu mainMenu;
        private NotifyIcon trayIcon;


        // maybe try and eliminate the need for this class down the road
        public TrayRadio() {
            mainMenu = new MainMenu(new RadioComponent(mainMenu));      // maybe don't need to pass in this mainMenu

            trayIcon = new NotifyIcon();
            trayIcon.Text = "MyTrayApp";
            trayIcon.Icon = new Icon(SystemIcons.Application, 40, 40);

            trayIcon.ContextMenuStrip = mainMenu;
            trayIcon.Visible = true;
        }

        // this hides the form (TrayRadio is actually a form)
        protected override void OnLoad(EventArgs e) {
            Visible = false;
            ShowInTaskbar = false;
            
            base.OnLoad(e);
        }
    }
}
