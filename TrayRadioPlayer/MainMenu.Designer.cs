﻿using System.Windows.Forms;

namespace TrayRadioPlayer {
    partial class MainMenu {

        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Design Code
        private void InitializeComponent() {
            // order the list in a logical order - most used buttons should be at the bottom (closer to where the mouse is on menu open)

            ToolStripMenuItem TechHouse = new ToolStripMenuItem();
            TechHouse.Text = "Di - Tech House";
            ToolStripMenuItem Liquid = new ToolStripMenuItem();
            Liquid.Text = "Di - Liquid DnB";
            ToolStripMenuItem Trance = new ToolStripMenuItem();
            Trance.Text = "Di - Trance";
            ToolStripMenuItem EpicTrance = new ToolStripMenuItem();
            EpicTrance.Text = "Di - Epic Trance";
            ToolStripMenuItem DubstepFM = new ToolStripMenuItem();
            DubstepFM.Text = "DubstepFM";

            // Stations List
            this.StationsList = new System.Windows.Forms.ToolStripMenuItem();
            this.StationsList.Name = "StationsList";
            this.StationsList.Text = "Stations";

            StationsList.DropDownItems.AddRange(new ToolStripMenuItem[] { TechHouse, Liquid, Trance, EpicTrance, DubstepFM });


            this.Items.Add(StationsList);


            // PlayStopBtn
            this.PlayStopBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.PlayStopBtn.Name = "PlayStopBtn";
            this.PlayStopBtn.Text = "Play/Stop";
            this.PlayStopBtn.Click += new System.EventHandler(this.PlayStopBtn_Click);
            this.Items.Add(PlayStopBtn);

            // VolumeControl
            this.VolumeControlBtn= new System.Windows.Forms.ToolStripMenuItem();
            this.VolumeControlBtn.Name = "VolumeControl";
            this.VolumeControlBtn.Text = "Volume";
            this.VolumeControlBtn.Click += new System.EventHandler(this.VolumeControlBtn_Click);
            this.Items.Add(VolumeControlBtn);


            // MuteBtn
            this.MuteBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.MuteBtn.Name = "MuteBtn";
            this.MuteBtn.Text = "Toggle Mute";
            this.MuteBtn.Click += new System.EventHandler(this.MuteBtn_Click);
            this.Items.Add(MuteBtn);



            // this should always be at the bottom
            // ExitBtn
            this.ExitBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.Click += new System.EventHandler(this.ExitApp);
            this.Items.Add(ExitBtn);
        }


        // order these in the order they're presented in the initialize method
        private System.Windows.Forms.ToolStripMenuItem ExitBtn;
        private System.Windows.Forms.ToolStripMenuItem PlayStopBtn;
        private System.Windows.Forms.ToolStripMenuItem StationsList;
        private System.Windows.Forms.ToolStripMenuItem MuteBtn;
        private System.Windows.Forms.ToolStripMenuItem VolumeControlBtn;

        #endregion
    }
}