﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrayRadioPlayer {
    public partial class MainMenu : System.Windows.Forms.ContextMenuStrip {

        private RadioComponent radio;

        public MainMenu(RadioComponent radio) {
            InitializeComponent();

            // allow the menu and the radio back-end to talk to each other directly
            this.radio = radio;
        }

        private void PlayStopBtn_Click(object sender, EventArgs e) {
            radio.PlayStop();
        }

        private void VolumeControlBtn_Click(object sender, EventArgs e) {
            // open volume control window
            Form VolumeControl = new VolumeControl(radio);      // allow the volume form to access the main radio component to change the volume directly
            VolumeControl.Visible = true;
        }

        private void MuteBtn_Click(object sender, EventArgs e) {
            radio.ToggleMute();
        }

        private void ExitApp(object sender, EventArgs e) {
            Application.Exit();
        }
    }
}